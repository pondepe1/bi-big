# BI-BIG

## Cviceni 3

### Poznamky
* dsecompose_seed_node_1... ID containeru se seed node (lze zjistit klasicky pomoci docker container ls)
* KEYSPACE... skupina tabulek se stejnym replikacnim faktorem
* NoSQL INSERT INTO
  * Pri vlozeni dvou zaznamu se stejnym primary key se puvodni zaznam presipuje (diky stejnemu hashi PK)

### Samostatna prace
* Zdroj dat: [Free CSV link](https://support.spatialkey.com/spatialkey-sample-csv-data/)
  * Musi se shodovat poradi sloupcu (nazvy nemuseji)
  * V keyspace je potreba vytvorit tabulku se strukturou odpovidajici strukture souboru CSV
  * Je potreba vytvorit script create-table.txt
  * Vytvorit tabulku spustenim CREATE scriptu
  * Vlozeni zaznamu z CSV pomoci COPY

## Cvičení 08
* Kibana queries
  * "size": počet vypsaných záznamů
  * "from": od kolikátého záznamu
  * "sort": řazení
  * "aggs": agregace

## Vypnutí PROXY pro Docker
	sudo -i
	rm /etc/systemd/system/docker.service.d/http-proxy.conf
	systemctl daemon-reload
	systemctl restart docker

## Zjištění IP adresy běžícího containeru
	docker inspect <container_id> | grep '"IPAddress":'

## Instalace Docker Compose
* curl -L https://github.com/docker/compose/releases/download/1.23.0-rc2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
* chmod +x /usr/local/bin/docker-compose
* docker-compose --version

## Docker
* Zastavení a odstranění všech containerů
	* docker kill $(docker ps -q)
	* docker rm $(docker ps -a -q)

## Semestrální test
* Podstatné téma: interní funkčnost Cassandry
* Teorie z probraných a vyvěšených přednášek
* Praktická část na to, co se dělalo na cvičení (např. Docker)

## Semestrální práce
* Každý dataset aspoň 50 000 záznamů
* Import datasetů do Spark
* Vytvořit agregace z datasetů
* Agregace potřeba uložit
* Indexace dat (Elastic Search)
* Vizualizace (Kibana)
* Dokumentace
  * Prakticky zaměřená, nerozebírat teorii, ale postup

## Bonusová úloha
* Najít smyluplná data
* Dostat je do Elasticsearch (import)
* Vizualizace dat
* Buď logstash nebo filebeat
* [Kibana Dashboard Examples] (https://www.google.cz/search?q=kibana+dashboard&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjWrYDa7ufeAhXLA8AKHeIEBAUQ_AUIDigB&biw=1920&bih=894)
* Vytvořit dashboard a vyexportovat
* Zaznamenat postup, zaslat postup a dashboard
* JSON generátor:
  * https://www.json-generator.com/
