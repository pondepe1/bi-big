# BI-BIG USECASE-1

## Zadání

Nastartujte si Spark cluster s Hadoopem a pomocí technologie Spark připravte řešení na následující otázky:
1\. Počet výskytů jednotlivých slov v souboru obama.txt (2b)
2\. Počet výskytů slova "America" v souboru obama.txt (3b)
3\. Počet výskytů písmene "x" v souboru obama.txt (3b)
4\. Nalezněte počet zákazníků, kteří nemají vyplněný telefon  (3b)
5\. Nalezněte počet zákazníků, kteří nemají žádnou objednávku (spojení je přes sloupec ZAKAZNIK_KEY) (5b)
6\. Nalezněte datum, kdy byla udělaná poslední objednávka (největší datum) (4b)

Konverzi na datum je potřeba nastavit v option

## Řešení

### Otázka 1

val file = sc.textFile("hdfs://172.17.0.4:9000/user/obama.txt");
val counts = file.flatMap(file => file.split(" ")).map(word => (word, 1)).reduceByKey(_ + _);
counts.collect().foreach(println);

### Otázka 2

val file = sc.textFile("hdfs://172.17.0.4:9000/user/obama.txt");
val counts = file.flatMap(file => file.split(" ")).filter(value=>value=="America").map(word => (word, 1)).reduceByKey(_ + _);
counts.collect().foreach(println);

### Otázka 3

val file = sc.textFile("hdfs://172.17.0.4:9000/user/obama.txt");
val counts = file.flatMap(line=>line.split("").filter(value=>value=="x").map(char=>(char,1))).reduceByKey(_ + _)
counts.collect().foreach(println);

### Otázka 4+5+6

val obj = spark.sqlContext.read.format("csv").option("header", "true").option("delimiter", "\\t").option("dateFormat", "dd.mm.yyyy").option("inferSchema", "true").load("hdfs://172.17.0.4:9000/user/obj.txt")
val zak = spark.sqlContext.read.format("csv").option("header", "true").option("delimiter", ";").option("inferSchema", "true").load("hdfs://172.17.0.4:9000/user/zak.txt")

obj.registerTempTable("OBJEDNAVKA")
zak.registerTempTable("ZAKAZNIK")

spark.sqlContext.sql("select count(1) from ZAKAZNIK where TELEFON=''").show()
spark.sqlContext.sql("select count(1) from ZAKAZNIK z left join OBJEDNAVKA o on o.ZAKAZNIK_KEY=z.ZAKAZNIK_KEY where o.OBJEDNAVKA_KEY is null").show()
spark.sqlContext.sql("select max(DATUM) from OBJEDNAVKA").show()
