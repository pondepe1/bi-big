# UseCase 2

## Poznámky

Dataset umístit do logstash/datasets/

Připravit odpovídající logstash/pipeline/logstash.conf

## Příkazy

Logy z logstashe (stav importu):

    sudo docker logs -f logstash

Smazat všechny registované kontejnery:

    sudo docker rm $(sudo docker ps -a -q)

Zjistit IP adresu kontejneru:

    sudo docker inspect <container_id/name> | grep '"IPAddress":'

Kibana:

    http://localhost:5601
