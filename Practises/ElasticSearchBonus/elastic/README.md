# ElasticSearch, LogStash, Kibana

## Instalace LogStash

Ke zprovoznění příkladu je potřeba nejprve nainstalovat LogStash.

    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
    sudo apt-get install apt-transport-https
    echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
    sudo apt-get update && sudo apt-get install logstash

## Spuštění Docker Containerů

Po nainstalování LogStashe spusťte kontejnery Dockeru (ideálně na pozadí).

Je potřeba mít nainstalovaný docker-compose.

    sudo docker-compose up -d

Ze spuštěných kontejnerů nás zajímají kontejnery pro ElasticSearch a Kibanu.

Pro test funkčnosti navštivte adresy:
* REST rozhraní serveru ElasticSearch: [http://127.0.0.1:9200](http://127.0.0.1:9200)
* Rozhraní Kibany: [http://127.0.0.1:5601](http://127.0.0.1:5601)

Pokud vše proběhlo správně, server Elasticu by měl vrátit podobné inforormace v JSONu:

    {  
        "name" : "Hb6zwa5",  
        "cluster_name" : "docker-cluster",  
        "cluster_uuid" : "hHmOniM1QSG6b7clObXWEQ",  
        "version" : {  
            "number" : "6.2.4",  
            "build_hash" : "ccec39f",  
            "build_date" : "2018-04-12T20:37:28.497551Z",  
            "build_snapshot" : false,  
            "lucene_version" : "7.2.1",  
            "minimum_wire_compatibility_version" : "5.6.0",  
            "minimum_index_compatibility_version" : "5.0.0"  
        },  
        "tagline" : "You Know, for Search"  
    }  

V případě Kibany by se mělo zobrazit její grafické rozhraní.

## Import mappingu do ElasticSearch

Nyní je potřeba sdělit Elasticu, jakou strukturu a formát budou mít importovaná data.

    curl --noproxy '*' -XPUT -H 'Content-Type: application/json' '127.0.0.1:9200/_template/applications' -d @applications_template.json

Kontrolu importu mappingu lze provést na adrese [http://localhost:9200/_template/applications](http://localhost:9200/_template/applications).

Zobrazit by se měl JSON odpovídající importované šabloně.

## Spuštění LogStash z příkazové řádky

Dalším krokem je samotný import dat do Elasticu za využití LogStashe.

Cesta ke spustitelnému souboru LogStashe vychází z layoutu pro LogStash popsaného v dokumentaci.

    sudo /usr/share/logstash/bin/logstash -f logstash_applications.conf

Při importu se zároveň vytváří dump importovaných záznamů (soubor logstash_dump.txt).

## Tvorba Index Pattern

Data byla importována do Elasticu. Nyní přejděte do Kibany a v záložce Management vytvořte Index Pattern.

* Záložka Management
* Index Patterns
* Create Index Pattern: jako jméno zvolte logstash*
* Jako časový filtr zvolte last_updated

## Zastavení Containerů

Pro zastavení spuštěných kontejnerů zadejte příkaz:

    sudo docker-compose down

## Struktura příkladu

* docker-compose.yml
    * Konfigurace Docker Containerů  
* googleplaystore.csv
    * Zdrojový dataset
* logstash_applications.conf
    * Konfigurační soubor pro LogStash
    * Je zde nastaven vstup pro LogStash
    * Nastaveny filtry pro typ souboru, strukturu souboru a datové typy
    * Nastaven výstup pro LogStash
* applications_template.json
    * Mapping zdrojových dat pro ElasticSearch
* attachments
    * Adresář obsahující přílohy
    * Export objektů z Kibany (dashboard + vizualizace) a screenshoty Dashboardu

## Zdroje

### Dataset

[Kaggle: Google Play Store Apps](https://www.kaggle.com/lava18/google-play-store-apps)

### Dokumentace Logstash

[LogStash: Directory Layout](https://www.elastic.co/guide/en/logstash/current/dir-layout.html)  
[LogStash: Running From Command-Line](https://www.elastic.co/guide/en/logstash/current/running-logstash-command-line.html)  
[LogStash: Configuration](https://www.elastic.co/guide/en/logstash/current/configuration.html)  
[Logstash: Filters](https://www.elastic.co/guide/en/logstash/current/filter-plugins.html)  
[LogStash CSV Import Tutorial](https://www.youtube.com/watch?v=rKy4sFbIZ3U&t=827s)
